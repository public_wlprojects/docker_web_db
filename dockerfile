# DF version web6
# changed ububtu version to latest
# installing iputils-ping tool

FROM ubuntu:20.04
ENV DEBIAN_FRONTEND=noninteractive
ADD web_data /var/www/html/
RUN apt-get update
RUN apt-get install apache2 -y
RUN apt-get install php -y
RUN apt-get install php-mysql -y

EXPOSE 80
CMD ["apachectl", "-D", "FOREGROUND"]
