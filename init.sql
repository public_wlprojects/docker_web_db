CREATE DATABASE dev_to;
use dev_to; 
CREATE TABLE test(
    id int NOT NULL AUTO_INCREMENT,
    name varchar(255),
    email varchar(255),
    message text,
    PRIMARY KEY (id)
    );
